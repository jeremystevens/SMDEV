import flask
from flask import Flask, render_template, request, url_for, redirect, flash, session, send_file, Response, abort
from markupsafe import escape
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Boolean, DateTime
from sqlalchemy.sql import func

''''
  =====================================
            General Setup
  ====================================
'''
app = Flask(__name__)
app.secret_key = '7139abd5380e6aa76084caf01740e1f4a4d96b9fcc9e27b36017dccbcdd80c10'
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://cgdev:123@localhost/cablegram"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SESSION_PERMANENT"] = False
app.config['SESSION_TYPE'] = 'filesystem'
db = SQLAlchemy(app)

''''
  =====================================
            Users Model
  ====================================
'''


class Users(db.Model):
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    phone = db.Column(db.VARCHAR(16))
    email = db.Column(db.VARCHAR(255))
    username = db.Column(db.VARCHAR(25))
    password_hash = db.Column(db.VARCHAR(40))
    first_name = db.Column(db.VARCHAR(20))
    last_name = db.Column(db.VARCHAR(20))
    middle_name = db.Column(db.VARCHAR(16))
    verification_code = db.Column(db.VARCHAR(16))
    verified = db.Column(Boolean)
    is_active = db.Column(Boolean)
    is_reported = db.Column(Boolean)
    is_blocked = db.Column(Boolean)
    created_at = db.Column(DateTime(timezone=True), server_default=func.now())
    updated_at = db.Column(DateTime(timezone=True), onupdate=func.now())

