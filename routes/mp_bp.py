from flask import Blueprint
# import controllers from MainPage
from controllers.MainPageController import about, signup, register, index, login


''''
  =====================================
            Get Request Page
  ====================================
'''

"""Blueprint for index."""
index_bp = Blueprint('mp_bp', __name__)
index_bp.route('/', methods=['GET'])(index)

"""Blueprint for About page."""
about_bp = Blueprint('about_bp', __name__)
about_bp.route('/about')(about)


''''
  =====================================
       Process Login / Registration
  ====================================
'''
"""Blueprint for Login"""
login_bp = Blueprint('login_bp', __name__)
login_bp.route('/login', methods=['GET', 'POST'])(login)

"""Blueprint for registration."""
register_bp = Blueprint('register_bp', __name__)
register_bp.route('/signup', methods=['GET'])(signup)
register_bp.route('/reg', methods=['GET', 'POST'])(register)
