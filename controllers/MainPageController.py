# Standard Libs
import sys
import datetime

# 3rd Party Libs
from flask import render_template, redirect, url_for, request, abort, flash, session
from models.User import Users
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
# local imports
from controllers.helpers import verification_code
from routes.pro_bp import pro_bp
from models.User import Users

db = SQLAlchemy()

""" 
=======================================
         Main Page Controllers 
========================================
"""


# Show Index Page
def index():
    return render_template('index.html')


# Show About Page
def about():
    return render_template('about.html')
    pass


# Signup (not needed with new HTML) leaving in case something changes
def signup():
    return render_template('signup.html')


""" 
==========================================
       Login / Registration Controllers
==========================================
"""


# Process Logins
def login():
    if request.method == "POST":
        email = request.form['email']
        password = request.form['password']
        # need this to Prevent Errors if Email isn't found
        try:
            # u = Users.query.filter_by(email=email).first()
            # result = u.check_password(password)
            result = Users.query.filter(Users.email == email and Users.password == password).first()
            if result:
                session['user_name'] = email
                # save login logs to file
                now = datetime.datetime.now()
                today_date = now.strftime("%m/%d/%Y")
                time = now.strftime("%I:%M:%S")
                file1 = open("login_logs.txt", "a")  # append mode
                file1.write(
                    "Date: " + today_date + " Time: " + time + " Email: " + email + " Status: Successful " + "\n")
                file1.close()
                # redirect to profile page
                return redirect(url_for('pro_bp.profile', ))
            else:
                # log failed login attempts
                now = datetime.datetime.now()
                today_date = now.strftime("%m/%d/%Y")
                time = now.strftime("%I:%M:%S")
                file1 = open("login_logs.txt", "a")  # append mode
                file1.write(
                    "Date: " + today_date + " Time: " + time + " Email: " + email + " Status: Failed Login " + "\n")
                file1.close()
                # show error message to user
                flash('Incorrect username or password. Please try again.')
                return redirect(url_for('mp_bp.index', ))
        except AttributeError:
            # log Errors to file
            now = datetime.datetime.now()
            today_date = now.strftime("%m/%d/%Y")
            time = now.strftime("%I:%M:%S")
            file1 = open("errors_log.txt", "a")  # append mode
            file1.write(
                "Date: " + today_date + " Time: " + time + " Email: " + email + " Status: AttributeError " + "\n")
            file1.close()
            flash('Incorrect username or password. Please try again.')
            return redirect(url_for('mp_bp.index', ))


# Process Registration
def register():
    if request.method == "POST":
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        user = Users.query.filter_by(email=email).first()
        if not user:
            password = request.form['password']
            password = generate_password_hash(password)
            # generate a verification code 5-6 chars
            code = verification_code(5, 6)
            process = Users(email=email, password_hash=password, first_name=first_name, last_name=last_name,
                            verification_code=code)
            db.session.add(process)
            db.session.commit()
            flash('Your account was created. you may now login')
            return redirect(url_for('mp_bp.index', ))
        # if email is already registered send back to index with error
        else:
            flash('An account with that email already Exists')
            return redirect(url_for('mp_bp.index', ))
