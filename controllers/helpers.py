import string
import random
import rstr
from werkzeug.security import generate_password_hash, check_password_hash

""" 
======================================
         Generate Verification Code
========================================
"""


def verification_code(min_size, max_size):
    letters = string.ascii_uppercase
    lower_letters = string.ascii_lowercase
    random_string = rstr.rstr(lower_letters + letters, min_size, max_size)
    return random_string
