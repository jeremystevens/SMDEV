# Standard Libs
import sys

# 3rd Party Libs
from flask import render_template, redirect, url_for, request, abort, flash, session
from models.Profile import Profile
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import flask_login
from flask_login import fresh_login_required

# Local Imports
from controllers.helpers import verification_code

from routes import mp_bp
db = SQLAlchemy()

""" 
=======================================
         Profile Controllers 
========================================
"""


# Profile
def profile():
    if 'user_name' in session:
        return render_template('profile.html')
    else:
        # redirect back to page index
        return redirect(url_for('mp_bp.index', ))
