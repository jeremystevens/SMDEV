#  Std Libs

# 3rd Party Imports
from flask import Flask, render_template, request
from flask_migrate import Migrate
from flasgger import Swagger
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import flask_login
# Local Imports
from models.User import db
from routes.mp_bp import register_bp, about_bp, index_bp, login_bp
from routes.pro_bp import pro_bp

''''
  =====================================
            General Setup 
  ====================================
'''

db = SQLAlchemy()
app = Flask(__name__)
app.config.from_object('config')
swagger = Swagger(app)
db.init_app(app)
login_manager = LoginManager(app)

''''
  =====================================
            Login Manager
  ====================================
'''


class User(flask_login.UserMixin):
    pass


@login_manager.user_loader
def user_loader(email):
    pass


@login_manager.request_loader
def request_loader(request):
    pass


@login_manager.unauthorized_handler
def unauthorized_handler():
    return '404'


''''
  =====================================
            MainPage BP
  ====================================
'''

"""Blueprint for index."""
app.register_blueprint(index_bp)
"""Blueprint for About page."""
app.register_blueprint(about_bp)
"""Blueprint for registration."""
app.register_blueprint(register_bp)
"""Blueprint for Login"""
app.register_blueprint(login_bp)

''''
  =====================================
            Profile BP
  ====================================
'''
"""Blueprint for Profile Page"""
app.register_blueprint(pro_bp)
