import datetime

from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field
from marshmallow import fields
from sqlalchemy import Date
from config import *
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Boolean, DateTime
from sqlalchemy.sql import func
from werkzeug.security import generate_password_hash, check_password_hash

db = SQLAlchemy()


class Users(db.Model):
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    phone = db.Column(db.VARCHAR(16))
    email = db.Column(db.VARCHAR(255))
    username = db.Column(db.VARCHAR(25))
    password_hash = db.Column(db.VARCHAR(40))
    first_name = db.Column(db.VARCHAR(20))
    last_name = db.Column(db.VARCHAR(20))
    verification_code = db.Column(db.VARCHAR(16))
    verified = db.Column(Boolean)
    is_active = db.Column(Boolean)
    is_reported = db.Column(Boolean)
    is_blocked = db.Column(Boolean)
    created_at = db.Column(DateTime(timezone=True), server_default=func.now())
    updated_at = db.Column(DateTime(timezone=True), onupdate=func.now())

    def __init__(self, email, password):
        self.email = email
        self.password = password
    # below our user model, we will create our hashing functions

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
