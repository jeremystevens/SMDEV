import datetime

from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field
from marshmallow import fields
from sqlalchemy import Date
from config import *
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Boolean, DateTime
from sqlalchemy.sql import func
from werkzeug.security import generate_password_hash, check_password_hash

''''
  =====================================
            Profile Model
  ====================================
'''

db = SQLAlchemy()


class Profile(db.Model):
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    about_me = db.Column(db.VARCHAR(255))
    activities = db.Column(db.VARCHAR(255))
    birthday = db.Column(db.VARCHAR(25))
    favorite_book = db.Column(db.VARCHAR(25))
    first_name = db.Column(db.VARCHAR(25))
    last_name = db.Column(db.VARCHAR(25))
    favorite_movie = db.Column(db.VARCHAR(25))
    favorite_music = db.Column(db.VARCHAR(255))
    favorite_quotes = db.Column(db.VARCHAR(255))
    favorite_tv_shows = db.Column(db.VARCHAR(255))
    home_location = db.Column(db.VARCHAR(255))
    current_location = db.Column(db.VARCHAR(255))
    interests = db.Column(db.VARCHAR(255))
    picture_bg_url = db.Column(db.VARCHAR(255))
    picture_sm_url = db.Column(db.VARCHAR(255))
    Profile_Gender = db.Column(db.VARCHAR(25))
    Looking_for = db.Column(db.VARCHAR(25))
    political_views = db.Column(db.VARCHAR(255))
    school_count = db.Column(db.VARCHAR(25))
    religion = db.Column(db.VARCHAR(255))
    sig_other_id = db.Column(db.VARCHAR(255))
    update_time = db.Column(DateTime(timezone=True), onupdate=func.now())
    userid = db.Column(db.VARCHAR(25))
    wall_count = db.Column(db.VARCHAR(255))
    webpage_link = db.Column(db.VARCHAR(255))
    web_poke_link = db.Column(db.VARCHAR(25))
    web_profile_link = db.Column(db.VARCHAR(255))
    web_send_msg_link = db.Column(db.VARCHAR(255))
    work_place_count = db.Column(db.VARCHAR(255))
    work_place_list = db.Column(db.VARCHAR(255))
